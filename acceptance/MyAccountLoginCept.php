<?php 
$i = new AcceptanceTester($scenario);
$i->wantTo('Login as tester order');
$i->amOnPage('/myaccount/login/');
$i->fillField('username', 'apw_tester_order@yahoo.com');
$i->fillField('password', 'usap1q2w');
$i->click('Login');
$i->see('Hi, APW TESTER!');
