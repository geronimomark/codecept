<?php


class Jcw_10382Cest
{
    public function _before(AcceptanceTester $i)
    {
    }

    public function _after(AcceptanceTester $i)
    {
    }

    // tests
    public function testIneligibleCoupons(AcceptanceTester $i)
    {
        $i->wantTo('Have error message on ineligible coupons');
        $i->amOnPage('/?jcwcoupon=JCWLBJ23');
        $i->maximizeWindow();
        $i->fillField('Ntt', 'Wax');
        $i->click('#nttsubmit');
        $i->click("MEGUIAR'S UNIVERSAL WAX");
        $i->waitForJs("return $.active == 0;", 10);
        $i->click("ADD TO CART");
        $i->waitForElement('#coupon_code', 30);
        $i->see('The coupon JCWLBJ23 has');
        $i->dontSee('JCWLBJ23 has been applied.');
    }

    public function testInvalidCoupons(AcceptanceTester $i)
    {
        $i->wantTo('Have error message on invalid coupons');
        $i->amOnPage('/?jcwcoupon=12345');
        $i->maximizeWindow();
        $i->fillField('Ntt', 'Wax');
        $i->click('#nttsubmit');
        $i->click("MEGUIAR'S UNIVERSAL WAX");
        $i->waitForJs("return $.active == 0;", 10);
        $i->click("ADD TO CART");
        $i->waitForElement('#coupon_code', 30);
        $i->see('Coupon Code 12345 is invalid.');
        $i->dontSee('12345 has been applied.');
    }

    public function testValidCoupons(AcceptanceTester $i)
    {
        $i->wantTo('Have valid coupons');
        $i->amOnPage('/?jcwcoupon=WELCOME10');
        $i->maximizeWindow();
        $i->fillField('Ntt', 'Wax');
        $i->click('#nttsubmit');
        $i->click("MEGUIAR'S UNIVERSAL WAX");
        $i->waitForJs("return $.active == 0;", 10);
        $i->click("ADD TO CART");
        $i->waitForElement('#coupon_code', 30);
        $i->see('WELCOME10 has been applied.');
        $i->dontSee('Coupon Code WELCOME10 is invalid.');
        $i->dontSee('The coupon WELCOME10 has');
    }
}
