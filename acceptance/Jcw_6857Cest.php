<?php


class Jcw_6857Cest
{
    public function _before(AcceptanceTester $i)
    {
    }

    public function _after(AcceptanceTester $i)
    {
    }

    // tests
    public function tryToTest(AcceptanceTester $i)
    {
        $i->wantTo('Add vehicle in my account garage');
        $i->amOnPage('/');
        $i->maximizeWindow();
        $i->waitForJs("return $.active == 0;", 10);
        $i->waitForElement('li.hl-account', 10);
        $i->see('My Account');
        $i->click('li.hl-account');
        $i->see('Sign In');
        $i->click('Sign In');
        $i->see('Log In');
        $i->fillField('username', 'tester_order@yahoo.com');
        $i->fillField('password', 'usap1q2w');
        $i->click('#signin');
        $i->waitForJs("return $.active == 0;", 10);
        $i->click('My Garage');
        $i->waitForJs("return $.active == 0;", 10);
        $i->click('a.remove');
        $i->waitForJs("return $.active == 0;", 10);
        $i->click('#ymm_year');
        $i->selectOption('#ymm_year', '2015');
        $i->waitForJs("return $.active == 0;", 10);
        $i->click('#ymm_make');
        $i->selectOption('#ymm_make', '1:Acura');
        $i->waitForJs("return $.active == 0;", 10);
        $i->click('#ymm_model');
        $i->selectOption('#ymm_model', '63012:ILX');
        $i->waitForJs("return $.active == 0;", 10);
        $i->click('#ymm_submodel');
        $i->selectOption('#ymm_submodel', '2:Base');
        $i->waitForJs("return $.active == 0;", 10);
        $i->click('#ymm_submit');
        $i->wait(30);
        $i->see('2015 Acura ILX Base');
    }
}
