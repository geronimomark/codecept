<?php
/**
 * SemSerpCest.php
 *
 * @category Test
 * @package  Acceptance
 * @author   Mark Anthony Geronimo <mark_geronimo@rocketmail.com>
 * @license  http://www.autopartswarehouse.com APW
 * @link     http://www.autopartswarehouse.com
 */

/**
 * SemSerpCest Class
 *
 * @category Test
 * @package  Acceptance
 * @author   Mark Anthony Geronimo <mark_geronimo@rocketmail.com>
 * @license  http://www.autopartswarehouse.com APW
 * @link     http://www.autopartswarehouse.com
 */
class SemSerpCest
{
    /**
     * [_before description]
     *
     * @param AcceptanceTester $i [description]
     *
     * @return [type]              [description]
     */
    private function _before(AcceptanceTester $i)
    {
    }

    /**
     * [_after description]
     *
     * @param AcceptanceTester $i [description]
     *
     * @return [type]              [description]
     */
    private function _after(AcceptanceTester $i)
    {
    }

    // SEM SERP TESTS 2016-05-19 markG
    /**
     * [pageLimitation description]
     *
     * @param AcceptanceTester $i [description]
     *
     * @return [type]              [description]
     */
    public function pageLimitation(AcceptanceTester $i)
    {
        $i->wantTo('Remove items/widgets from SEM SERP');
        $i->amOnPage('/part_types?part_type=Mirror');
        $i->dontSee('Your Recent Searches');
        $i->dontSeeLink('Universal');
        $i->resizeWindow(768, 400);
        $i->dontSee('Add to Cart');
    }

    /**
     * [headline description]
     *
     * @param AcceptanceTester $i [description]
     *
     * @return [type]              [description]
     */
    public function headline(AcceptanceTester $i)
    {
        $i->wantTo('Have new Headline for SEM SERP');
        $i->amOnPage('/part_types?part_type=Mirror&brand=Kool Vue');
        $i->see('Find Kool Vue Mirror');
        $i->amOnPage('/part_types?part_type=Mirror');
        $i->see('Find Mirror');
        $i->amOnPage('/part_types?brand=Kool Vue');
        $i->see('Find Kool Vue Parts');
        $i->amOnPage('/part_types?search=DG44EL');
        $i->see('DG44EL');
        $i->amOnPage('/part_types?part_type=Mirror&make=Honda&model=Civic&year=2010');
        $i->see('Find Mirror for your 2010 Honda Civic');
        $i->amOnPage('/part_types?part_type=Mirror&make=Honda');
        $i->see('Find Mirror for your Honda');
        $i->amOnPage('/part_types?part_type=Mirror&make=Honda&model=Civic');
        $i->see('Find Mirror for your Honda Civic');
        $i->amOnPage('/part_types?part_type=Mirror&make=Honda&year=2010');
        $i->see('Find Mirror for your 2010 Honda');
        $i->amOnPage('/part_types?part_type=Mirror&year=2010');
        $i->dontSee('Find Mirror for your 2010');
        $i->amOnPage('/part_types?part_type=Mirror&model=Civic');
        $i->dontSee('Find Mirror for your Civic');
        $i->amOnPage('/part_types?part_type=Mirror&model=Civic&year=2010');
        $i->dontSee('Find Mirror for your 2010 Civic');
    }

    /**
     * [seo description]
     *
     * @param AcceptanceTester $i [description]
     *
     * @return [type]              [description]
     */
    public function seo(AcceptanceTester $i)
    {
        $i->wantTo('Add Robots meta');
        $i->amOnPage('/part_types?part_type=Mirror');
        $i->seeInSource('<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOSNIPPET">');
    }

    /**
     * [tollFree description]
     *
     * @param AcceptanceTester $i [description]
     *
     * @return [type]              [description]
     */
    public function tollFree(AcceptanceTester $i)
    {
        $i->wantTo('Have new Toll Free Number');
        $i->amOnPage('/part_types?part_type=Mirror&tvmecrof=b');
        $i->waitForJs("return $('#h-letstalk-tollfree').text() == '1-800-481-2635'", 10);
        $i->waitForJs("return $('#h-letstalk-toll').text() == '1-312-542-1415'", 10);

        $i->amOnPage('/part_types?part_type=Mirror&tvmecrof=a');
        $i->waitForJs("return $('#h-letstalk-tollfree').text() == '1-888-279-0864'", 10);
        $i->waitForJs("return $('#h-letstalk-toll').text() == '1-312-431-6274'", 10);
    }

    /**
     * [sortingAndPagination description]
     *
     * @param AcceptanceTester $i [description]
     *
     * @return [type]              [description]
     */
    public function sortingAndPagination(AcceptanceTester $i)
    {
        $i->wantTo('Show only direct fit and no grid view');
        $i->amOnPage('/part_types?part_type=Mirror');
        $i->dontSeeElement('.icon-grid');
    }

    /**
     * [vehicleSelector description]
     *
     * @param AcceptanceTester $i [description]
     *
     * @return [type]              [description]
     */
    public function vehicleSelector(AcceptanceTester $i)
    {
        $i->wantTo('Have Left Navigation Vehicle Selector');
        $i->amOnPage('/?tvmecrof=b');
        $i->amOnPage('/part_types?part_type=Mirror');
        $i->maximizeWindow();
        $i->seeElement('h3', ['data-elemname' => 'partfinder_title_text']);
        $i->dontSee('add new');
        $i->click('#PartfinderFilterContainer .ymmse-year .icon-arrow-down');
        $i->waitForElementVisible('#PartfinderFilterContainer div.ymmse-year div.ndropdown-custom-ddwn', 3);
        $i->see('1970');
        $i->dontSee('1969');
        $i->click('#PartfinderFilterContainer div.ymmse-year div.ndropdown-custom-ddwn li[data-elemname="year_value_8"]');
        $i->waitForJs("return $.active == 0;", 60);
        $i->waitForElementVisible('#PartfinderFilterContainer div.ymmse-make div.ndropdown-custom-ddwn', 3);
        $i->see('Honda');
        $i->click('#PartfinderFilterContainer div.ymmse-make div.ndropdown-custom-ddwn li[data-elemname="make_value_14"]');
        $i->waitForJs("return $.active == 0;", 60);
        $i->waitForElementVisible('#PartfinderFilterContainer div.ymmse-model div.ndropdown-custom-ddwn', 3);
        $i->see('Civic');
        $i->click('#PartfinderFilterContainer div.ymmse-model div.ndropdown-custom-ddwn li[data-elemname="model_value_4"]');
        $i->click('button.searchVehiclePF');
        $i->wait(5);
        $i->seeCurrentUrlEquals('/part_types?part_type=Mirror&make=Honda&model=Civic&year=2010');
        $i->waitForJs("return $.active == 0;", 60);
        $i->click('#primaryDeleter');
        $i->wait(5);
        $i->seeCurrentUrlEquals('/part_types?part_type=Mirror');
    }
}
