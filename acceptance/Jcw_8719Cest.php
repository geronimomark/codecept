<?php


class Jcw_8719Cest
{
    public function _before(AcceptanceTester $i)
    {
    }

    public function _after(AcceptanceTester $i)
    {
    }

    // tests
    public function testMatchMode(AcceptanceTester $i)
    {
        $i->wantTo('Have varying search_enhancement');
        $i->amOnPage('/');
        $i->maximizeWindow();
        $i->fillField('Ntt', '2010 Honda Civic Mirror');
        $i->click('#nttsubmit');
        $i->click('#ZFDebugInfo_hydra');
        $mvt = $i->grabCookie('jcw_ab100');
        if ($mvt == "b") {
            $i->see('search_enhancement":1');
        } else {
            $i->dontSee('search_enhancement":1');
        }
    }

    public function testQueryMap(AcceptanceTester $i)
    {
        $i->wantTo('Have Part Name in Left Nav');
        $i->amOnPage('/');
        $i->maximizeWindow();
        $i->fillField('Ntt', 'Hood');
        $i->click('#nttsubmit');
        $i->see('SELECTED FILTERS');
        $i->see('JCW Part: Hood');
    }

    public function testQueryMapNonPart(AcceptanceTester $i)
    {
        $i->wantTo('Dont have non-part name in Left Nav');
        $i->amOnPage('/');
        $i->maximizeWindow();
        $i->fillField('Ntt', 'Struts');
        $i->click('#nttsubmit');
        $i->dontSee('SELECTED FILTERS');
        $i->dontSee('JCW Part: Struts');
    }

}
