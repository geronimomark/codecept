<?php


class Stt_4576Cest
{
    public function _before(AcceptanceTester $i)
    {
    }

    public function _after(AcceptanceTester $i)
    {
    }

    // tests
    public function testGoogleTrustedLogoAndScript(AcceptanceTester $i)
    {
        $i->wantTo('Have Google Trusted Store Logo');
        $i->amOnPage('/');
        $i->seeElement('img.gts');
        $i->seeInSource('<script type="text/javascript" async="" src="http://www.googlecommerce.com/trustedstores/api/js"></script>');
        
        $i->amOnPage('/catalogs');
        $i->seeElement('img.gts');
        $i->seeInSource('<script type="text/javascript" async="" src="http://www.googlecommerce.com/trustedstores/api/js"></script>');
        
        $i->amOnPage('/customer/feedback');
        $i->seeElement('img.gts');
        $i->seeInSource('<script type="text/javascript" async="" src="http://www.googlecommerce.com/trustedstores/api/js"></script>');
        
        $i->amOnPage('/articles.aspx');
        $i->seeElement('img.gts');
        $i->seeInSource('<script type="text/javascript" async="" src="http://www.googlecommerce.com/trustedstores/api/js"></script>');
        
        $i->amOnPage('/gallery.aspx');
        $i->seeElement('img.gts');
        $i->seeInSource('<script type="text/javascript" async="" src="http://www.googlecommerce.com/trustedstores/api/js"></script>');
    }

    public function testOrderAndMerchantInfoInCheckout(AcceptanceTester $i)
    {
        $i->wantTo('Have Google Order and Merchant Information in Checkout');
        $i->amOnPage('/');
        $i->fillField('#ntt', 'wax');
        $i->click('#nttsubmit');
        $i->click('Universal Wax from Meguiars');
        $i->click('#addtocart');
        $i->waitForJs("return $.active == 0;", 60);
        $i->amOnPage('/checkout');
        // $i->submitForm('form[name="form_checkout2"]');
        // $i->click('input.btn-checkout');
        $i->waitForJs("return $.active == 0;", 60);
        // $i->submitForm(
            // 'form',
            // [
        $i->fillField('customer_first_name'           , 'test');
        $i->fillField('customer_last_name'            , 'test');
        $i->fillField('customer_street_address'       , 'test address');
        $i->fillField('customer_city'                 , 'test');
        $i->fillField('customer_postcode'             , '12345');
        $i->fillField('customer_phone_parts[one]'     , '111');
        $i->fillField('customer_phone_parts[two]'     , '111');
        $i->fillField('customer_phone_parts[three]'   , '1111');
        $i->fillField('customer_email_address'        , 'tester_order@yahoo.com');
        $i->fillField('cc_number'                     , '4111111111111111');
        $i->fillField('cc_cvv2'                       , '123');
        // $i->fillField('customer_state'                , 'AL');
        // $i->fillField('cc_type'                       , 'VISA');
        // $i->fillField('cc_expmonth'                   , '2');
        // $i->fillField('cc_expyear'                    , '2017');
        $i->click('#customer_state');
        $i->click('#customer_state option[value="AL"]');
        $i->click('#cc_type');
        $i->click('#cc_type option[value="VISA"]');
        $i->click('#cc_expmonth');
        $i->click('#cc_expmonth option[value="2"]');
        $i->click('#cc_expyear');
        $i->click('#cc_expyear option[value="2017"]');
            // ],
            // '#ctl00_Body_btnContinueCheckout'
        // );
        $i->click('#ctl00_Body_btnContinueCheckout');
        $i->waitForJs("return $.active == 0;", 60);
        $i->seeInSource('id="gts-order"');
        $i->seeInSource('class="gts-item"');
        $i->seeInSource('<span class="gts-i-prodsearch-store-id">238551');
        $i->seeInSource('<script type="text/javascript" async="" src="http://www.googlecommerce.com/trustedstores/api/js"></script>');
    }
}
