<?php


class Apw_11231Cest
{
    public function _before(AcceptanceTester $i)
    {
    }

    public function _after(AcceptanceTester $i)
    {
    }

    // tests
    public function enableQueryMap(AcceptanceTester $i)
    {
        $i->wantTo('Add request_uri in search service');
        $i->amOnPage('/search/?Ntt=mirror&searchType=global&shopId=1&N=0&addfitment=1');
        $i->maximizeWindow();
        $i->click('#smartfitwidgetlightbox a.lb-close');
        $i->click('#PartfinderFilterContainer .ymmse-year .icon-arrow-down');
        $i->waitForElementVisible('#PartfinderFilterContainer div.ymmse-year div.ndropdown-custom-ddwn', 3);
        $i->see('2010');
        $i->click('#PartfinderFilterContainer div.ymmse-year div.ndropdown-custom-ddwn li[data-elemname="year_value_8"]');
        $i->waitForJs("return $.active == 0;", 60);
        $i->waitForElementVisible('#PartfinderFilterContainer div.ymmse-make div.ndropdown-custom-ddwn', 3);
        $i->see('Honda');
        $i->click('#PartfinderFilterContainer div.ymmse-make div.ndropdown-custom-ddwn li[data-elemname="make_value_15"]');
        $i->waitForJs("return $.active == 0;", 60);
        $i->waitForElementVisible('#PartfinderFilterContainer div.ymmse-model div.ndropdown-custom-ddwn', 3);
        $i->see('Civic');
        $i->click('#PartfinderFilterContainer div.ymmse-model div.ndropdown-custom-ddwn li[data-elemname="model_value_4"]');
        $i->click('button.searchVehiclePF');
        $i->waitForJs("return $.active == 0;", 60);
        $i->waitForElementVisible('#smartfitwidgetlightbox');
        $i->click('#smartfitwidgetlightbox a.lb-close');
        $i->click('#ZFDebugInfo_hydra');
        $i->wait(2);
        $url = $i->grabFromCurrentUrl();
        // $i->see('op=search&amp;data=%7B%22catalogSource%22%3A%22Endeca%22%2C%22searchKeyword%22%3A%22mirror%22%2C%22site%22%3A%22autopartswarehouse.com%22%2C%22navParams%22%3A%7B%22N%22%3A%220%22%2C%22Ns%22%3A%22%22%7D%2C%22request_uri%22%3A%22%5C%2Fsearch%5C%2F%253FNtt%3Dmirror%2526searchType%3Dglobal%2526shopId%3D1%2526N%3D0%2526addfitment%3D1%2526');
        $i->see('op=search');
        $i->see(urlencode($url));
    }
}
