<?php 

class MarkG
{
    public $obj;

    public function __construct($scenario)
    {
        $this->obj = new AcceptanceTester($scenario);
    }

    public function pageLimitation()
    {
        $this->obj->wantTo('Remove items/widgets from SEM SERP');
        $this->obj->amOnPage('/part_types?part_type=Mirror');
        $this->obj->dontSee('Your Recent Searches');
        $this->obj->dontSeeLink('Universal');
        $this->obj->resizeWindow(768, 400);
        $this->obj->dontSee('Add to Cart');        
    }

    public function headline()
    {
        $this->obj->wantTo('Have new Headline for SEM SERP');
        $this->obj->amOnPage('/part_types?part_type=Mirror&brand=Kool Vue');
        $this->obj->see('Find Kool Vue Mirror');
        $this->obj->amOnPage('/part_types?part_type=Mirror');
        $this->obj->see('Find Mirror');
        $this->obj->amOnPage('/part_types?brand=Kool Vue');
        $this->obj->see('Find Kool Vue Parts');
        $this->obj->amOnPage('/part_types?search=DG44EL');
        $this->obj->see('DG44EL');
        $this->obj->amOnPage('/part_types?part_type=Mirror&make=Honda&model=Civic&year=2010');
        $this->obj->see('Find Mirror for your 2010 Honda Civic');
        $this->obj->amOnPage('/part_types?part_type=Mirror&make=Honda');
        $this->obj->see('Find Mirror for your Honda');
        $this->obj->amOnPage('/part_types?part_type=Mirror&make=Honda&model=Civic');
        $this->obj->see('Find Mirror for your Honda Civic');
        $this->obj->amOnPage('/part_types?part_type=Mirror&make=Honda&year=2010');
        $this->obj->see('Find Mirror for your 2010 Honda');
        $this->obj->amOnPage('/part_types?part_type=Mirror&year=2010');
        $this->obj->dontSee('Find Mirror for your 2010');
        $this->obj->amOnPage('/part_types?part_type=Mirror&model=Civic');
        $this->obj->dontSee('Find Mirror for your Civic');
        $this->obj->amOnPage('/part_types?part_type=Mirror&model=Civic&year=2010');
        $this->obj->dontSee('Find Mirror for your 2010 Civic');        
    }

}

// $i = new AcceptanceTester($scenario);
// $i->wantTo('Remove items/widgets from SEM SERP');
// $i->amOnPage('/part_types?part_type=Mirror');
// $i->dontSee('Your Recent Searches');
// $i->dontSeeLink('Universal');
// $i->resizeWindow(768, 400);
// $i->dontSee('Add to Cart');

$tester = new MarkG($scenario);
$tester->pageLimitation();
// $tester->headline();
